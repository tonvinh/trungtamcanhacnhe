<?php
function medical_enqueue_styles() {

    $parent_style = 'parent-style';
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',get_stylesheet_directory_uri() . '/style.css',array( $parent_style ));
}
add_action( 'wp_enqueue_scripts', 'medical_enqueue_styles' );
if (function_exists('add_image_size')) {
    add_image_size('img_activity', 263, 200, true);
    add_image_size('img_activity_slider', 870, 372, true);
    add_image_size('img_activity_banner', 1348, 292, true);
    add_image_size('member_avatar', 271, 271, true);

}
function get_recent_post($post_id = '', $posttype = 'hoatdong', $number = 8, $parent = 0){
    $args = array(
        'posts_per_page'   => $number,
        'orderby'          => 'id',
        'order'            => 'DESC',
        'post_type'        => $posttype,
        'post_status'      => 'publish',
        'suppress_filters' => true,
        'post_parent' => $parent
    );
    if(!empty($post_id)) {
        $args['exclude'] = $post_id;
    }
    if(!empty($category_id)) {
        $args['category'] = $category_id;
    }

    return get_posts($args);
}

?>
