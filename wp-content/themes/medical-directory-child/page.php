<?php
get_header(); ?>
                
  <div class="blog-content pt60">
    <div class="container">
      <div class="row">
        <div class="col-md-9">



            <?php
              // Start the loop.
              while ( have_posts() ) : the_post();

                // Include the page content template.
                  get_template_part('templates/page/content', 'page');
              // End the loop.
              endwhile;
              ?>



            </div><!-- /.blog-main -->

            <?php get_sidebar(); ?>

        </div><!-- /.row -->
    </div> <!-- /.container -->
  </div>




<?php get_footer(); ?>
