<!-- ************************** Start Sidebar **************************** -->



<div class="col-md-3">
    <div class="uou-sidebar pt40 right_sidebar ">
        <?php
            $defaults = array(
                'theme_location'  => 'primary_navigation_right',
                'menu'            => '',
                'container'       => '',
                'container_class' => '',
                'container_id'    => '',
                'menu_class'      => '',
                'menu_id'         => '',
                'echo'            => true,
            );

            wp_nav_menu( $defaults );
        ?>
        <?php dynamic_sidebar( 'mainsidebar' ); ?>

    </div>
</div>


<!-- ************************** End Sidebar **************************** -->