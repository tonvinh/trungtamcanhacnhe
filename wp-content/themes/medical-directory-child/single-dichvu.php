<?php get_header(); ?>
<?php
$user_ID = get_current_user_id();
$user_id = $user_ID;
$id = get_the_ID();
$post_id_1 = get_post($id);
$post_id_1->post_title;
$banner = get_field('banner_activity');
?>

    <div class="breadcrumb-content">
        <?php
            if(!empty($banner)) {?>
                <img src="<?php echo $banner['sizes']['img_activity_banner']?>">
            <?php } elseif(has_post_thumbnail($id)) {
                the_post_thumbnail('img_activity_banner');
            } else {
                $banner = get_field('banner_default', 'options');
            ?>
                <img src="<?php echo $banner['sizes']['img_activity_banner']?>">
            <?php }
        ?>
        <div class="container">
            <h3>
                <?php
                the_title();
                ?>

            </h3>


        </div>
    </div>
    <div class="blog-content ">
        <div class="container">
            <div class="row">
                <?php
                $wp_iv_directories_URLPATH = wp_iv_directories_URLPATH;
                wp_enqueue_style('iv_directories-style-71', wp_iv_directories_URLPATH . 'assets/cube/css/cubeportfolio.css');
                wp_enqueue_style('single-hospital-style84', medicaldirectory_CSS . 'single-hospital.css', array(), $ver = false, $media = 'all');
                $wp_directory = new wp_iv_directories();
                while (have_posts()) :
                the_post();
                if (has_post_thumbnail()) {
                    $feature_image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'large');
                    if ($feature_image[0] != "") {
                        $feature_img = $feature_image[0];
                    }
                } else {
                    $feature_img = wp_iv_directories_URLPATH . "/assets/images/default-directory.jpg";

                }
                $currentCategory = wp_get_object_terms($id, 'hospital-category');
                $cat_link = '';
                $cat_name = '';
                $cat_slug = '';
                if (isset($currentCategory[0]->slug)) {
                    $cat_slug = $currentCategory[0]->slug;
                    $cat_name = $currentCategory[0]->name;
                    $cat_link = get_term_link($currentCategory[0], 'hospital-category');
                }
                ?>
                <div class="single-direcotry-page">

                    <div class="row">
                        <div class="col-md-9 col-md-push-3">
                            <div class="content slider">
                                <div class="cbp-slider">
                                    <ul class="cbp-slider-wrap">

                                        <?php
                                        //get_template_part( 'content', 'single' );


                                        $gallery_ids_array = get_field('img_action');

                                        $i = 1;
                                        foreach ($gallery_ids_array as $slide) {
                                            if ($slide != '') { ?>
                                                <li class="cbp-slider-item">
                                                    <img
                                                        src="<?php echo $slide['img_item']['sizes']['img_activity_slider']; ?> "
                                                        alt="<?php esc_html_e('image', 'medical-directory'); ?>">
                                                </li>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        if (sizeof($gallery_ids_array) < 1) {

                                            if (has_post_thumbnail($id)) {
                                                echo '<li class="cbp-slider-item">';
                                                echo get_the_post_thumbnail($id, 'large');
                                                echo '</li>';
                                            } else {
                                                ?>
                                                <li class="cbp-slider-item">
                                                    <img
                                                        src="<?php echo wp_iv_directories_URLPATH . "/assets/images/default-directory.jpg"; ?>"
                                                        alt="<?php esc_html_e('image', 'medical-directory'); ?>">
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title-content">
                                    <div class="doctor-hospital-title"><h1><?php the_title() ?></h1>
                                    </div>

                                </div>

                                <div class="conten-desc">
                                    <?php
                                    if ($wp_directory->check_reading_access('description', $id)) {
                                        ?>

                                        <div class="cbp-l-project-desc-text">
                                            <?php
                                            $content = apply_filters('the_content', get_the_content());
                                            $content = str_replace(']]>', ']]&gt;', $content);
                                            echo $content;

                                            ?>
                                        </div>

                                        <?php
                                    } else {
                                        echo get_option('_iv_visibility_login_message');

                                    }
                                    ?>

                                </div>
                            </div>

                            <?php
                            if ($wp_directory->check_reading_access('video', $id)) {

                                $video_vimeo_id = get_post_meta($id, 'vimeo', true);
                                $video_youtube_id = get_post_meta($id, 'youtube', true);
                                if ($video_vimeo_id != '' || $video_youtube_id != '') {
                                    ?>
                                    <div class="content">
                                        <div class="title-content">
                                            <div class="doctor-hospital-title">
                                                <h5><?php esc_html_e('Video', 'medical-directory'); ?></h5></div>
                                        </div>
                                        <div class="conten-desc">
                                            <?php
                                            $v = 0;
                                            $video_vimeo_id = get_post_meta($id, 'vimeo', true);
                                            if ($video_vimeo_id != "") {
                                                $v = $v + 1; ?>
                                                <iframe
                                                    src="https://player.vimeo.com/video/<?php echo esc_attr($video_vimeo_id); ?>"
                                                    width="100%" height="315px" frameborder="0"></iframe>
                                                <?php
                                            }
                                            ?>
                                            <br/>
                                            <?php
                                            $video_youtube_id = get_post_meta($id, 'youtube', true);
                                            if ($video_youtube_id != "") {
                                                echo($v == 1 ? '<hr>' : '');
                                                ?>
                                                <iframe width="100%" height="315px"
                                                        src="https://www.youtube.com/embed/<?php echo esc_attr($video_youtube_id); ?>"
                                                        frameborder="0" allowfullscreen></iframe>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>

                        </div> <!-- End col-md-9-->
                        <div class="col-md-3 col-md-pull-9">
                            <div class="medicaldirectory-sidebar">
                                <?php $other_active = get_recent_post($post->ID,'dichvu', -1);?>
                                <div class="sidebar-content">
                                    <div class="cbp-l-project-details-title">
                                        <span>Hoạt động khác</span>
                                    </div>
                                    <ul class="item_active_left cbp-l-project-details-list">
                                        <?php foreach($other_active as $item): ?>
                                            <li><a href="<?php echo get_permalink($item->ID)?>"><?php echo $item->post_title; ?></a> </li>
                                        <?php endforeach;?>
                                    </ul>

                                </div>
                                <div class="sidebar-content">
                                    <div class="cbp-l-project-details-title">
                                        <span> Liên hệ với chúng tôi </span></div>
                                    <?php
                                    if ($wp_directory->check_reading_access('contact us', $id)) {
                                        ?>
                                        <form action="" id="message-pop" name="message-pop" method="POST" role="form">
                                            <div class="cbp-l-grid-projects-desc">
                                                <input id="subject" name="subject" type="text"
                                                       placeholder="Tiêu đề"
                                                       class="cbp-search-input">
                                            </div>
                                            <div class="cbp-l-grid-projects-desc">
                                                <input name="email_address" id="email_address" type="text"
                                                       placeholder="Email"
                                                       class="cbp-search-input">
                                            </div>
                                            <div class="cbp-l-grid-projects-desc">
                                                <textarea name="message-content" id="message-content"
                                                          class="cbp-search-" cols="54" rows="4"
                                                          title="<?php esc_html_e('Please Enter Message', 'medical-directory'); ?>"
                                                          placeholder="Nội dung"></textarea>
                                            </div>
                                            <input type="hidden" name="dir_id" id="dir_id"
                                                   value="<?php echo esc_attr($id); ?>">
                                            <a onclick="send_message_iv();"
                                               class="btn btn-primary full-width">Gửi</a>
                                            <div id="update_message_popup"></div>
                                        </form>
                                        <?php
                                    } else {
                                        echo get_option('_iv_visibility_login_message');

                                    }
                                    ?>
                                </div>


                            </div>




                        </div>    <!--medicaldirectory-sidebar -->

                    </div><!-- End col-md-3-->


                </div>
            </div>


        </div>


        <?php
        endwhile;
        ?>


    </div>
    </div> <!--  end blog-single -->

    </div> <!-- end container -->


<?php
if (sizeof($gallery_ids_array) > 1) {
    wp_enqueue_script('iv_directories-ar-script-23', wp_iv_directories_URLPATH . 'assets/cube/js/jquery.cubeportfolio.min.js');
    wp_enqueue_script('iv_directories-ar-script-102', wp_iv_directories_URLPATH . 'assets/cube/js/meet-team.js');
    wp_enqueue_script('single-hospital-js', medicaldirectory_JS . 'single-hospital.js', array('jquery'), $ver = true, true);
    wp_localize_script('single-hospital-js', 'medicaldirectory_data', array('ajaxurl' => admin_url('admin-ajax.php'),
        'loading_image' => '<img src="' . medicaldirectory_IMAGE . 'loader2.gif">',
        'current_user_id' => get_current_user_id(),
        'login_message' => esc_html__('Please login to remove favorite', 'medical-directory'),
        'Add_to_Favorites' => esc_html__('Add to Favorites', 'medical-directory'),
        'Login_claim' => esc_html__('Please login to Claim The Listing', 'medical-directory'),
        'login_favorite' => esc_html__("Please login to add favorite", 'medical-directory'),
        'login_review' => esc_html__("Please login to add review", 'medical-directory'),
    ));
}
get_footer();
