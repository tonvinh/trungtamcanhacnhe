<?php
/**
 * Template Name: Activity Template
 *
 */

?>
<?php
get_header(); ?>

<div class="blog-content pt60">
    <div class="container">
        <div class="row">
            <div class="col-md-9">

                <div class="pb50 container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="categories-imgs text-center">
                                    <?php
                                    $data = get_recent_hoatdong('','',-1);
                                    if ( !empty($data) ) :
                                        $i=0;
                                        foreach ( $data as $post ) {
                                            setup_postdata($post);

                                                $feature_img_id = get_option('_cate_main_image_'.$term_parent->term_id);
                                                $feature_img=medicaldirectory_IMAGE.'default-hospital-category.jpg';
                                                $feature_image = get_the_post_thumbnail_url( $post, 'img_activity' );

                                                if(!empty($feature_image)){
                                                    $feature_img = $feature_image;                                                                                               }
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <a href="<?php the_permalink(); ?>" style="color:#000000;">
                                                        <div class="image-wrapper-content">
                                                            <img src="<?php echo $feature_img; ?>" class="home-category-img" alt="home category">

                                                            <div class="categories-wrap-shadow"></div>
                                                            <div class="inner-meta ">
                                                                <div><?php the_title(); ?></div>
                                                                <i class="fa fa-link"></i>
                                                            </div>
                                                        </div>
                                                        <span style="font-size:15px;"><?php the_title(); ?></span>

                                                    </a>
                                                </div>
                                                <?php

                                        }
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.blog-main -->

        </div><!-- /.row -->
    </div> <!-- /.container -->
</div>




<?php get_footer(); ?>
