<?php
/**
 * Template Name: Member Template
 *
 */


get_header();
the_post();
?>

<div class="blog-content pt60">
    <div class="container">
        <div class="row">
            <div class="col-md-9">

                <div class="pb50 container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="out-team-content pt30">
                                    <h4 class="text-center"><?php the_title(); ?></h4>
                                    <div class="row">
                                        <?php
                                            $member = get_field('member_list');
                                        if(!empty($member)):
                                            foreach($member as $item):
                                        ?>
                                            <div class="col-sm-4">
                                                <div class="uou-block-6a rounded"><img src="<?php echo $item['member_img']['sizes']['member_avatar']?>" alt="" />
                                                    <h6><?php echo $item['member_name']?></h6>
                                                    <span><?php echo $item['member_position']?></span><br>
                                                    Email: <?php echo $item['member_email']?> <br>
                                                    Phone: <?php echo $item['member_phone']?>
                                                </div>
                                                <!-- end .uou-block-6a -->
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    </div>
                                </div>
                                <!-- end our-team -->
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.blog-main -->

        </div><!-- /.row -->
    </div> <!-- /.container -->
</div>




<?php get_footer(); ?>
