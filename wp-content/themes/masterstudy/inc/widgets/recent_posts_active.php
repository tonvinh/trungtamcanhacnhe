<?php

class Stm_Recent_Posts_Category extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'stm_recent_posts_category', // Base ID
			__('Recent posts activity', 'stm_domain'), // Name
			array( 'description' => __( 'Theme recent posts widget', 'stm_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$output = apply_filters( 'widget_output', $instance['output'] );
		
		if(empty($output) or !isset($output)) {
			$output = 3;
		};

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . esc_html( $title ) . $args['after_title'];
		}
		$query = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => $output, 'category__in' => array(26,27,28,29) ) );
		if($query->have_posts()): ?>
			<ul>
			<?php while($query->have_posts()): $query->the_post(); ?>
				<li>
					<a href="<?php the_permalink() ?>">
						<?php the_title(); ?>
					</a>
				</li>
			<?php endwhile; ?>
			</ul>
			<?php wp_reset_postdata(); ?>
		<?php endif;
	
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$title = '';
		$output = '';

		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}else {
			$title = __( 'Hoạt động', 'stm_domain' );
		}
		
		if ( isset( $instance[ 'output' ] ) ) {
			$output = $instance[ 'output' ];
		}else {
			$output = __( '3', 'stm_domain' );
		}

		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'stm_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'output' ) ); ?>"><?php _e( 'Output number:', 'stm_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'output' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'output' ) ); ?>" type="number" value="<?php echo esc_attr( $output ); ?>">
		</p>
	<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? esc_attr( $new_instance['title'] ) : '';
		$instance['output'] = ( ! empty( $new_instance['output'] ) ) ? esc_attr( $new_instance['output'] ) : '';

		return $instance;
	}

}

function register_stm_recent_posts_category_widget() {
	register_widget( 'Stm_Recent_Posts_Category' );
}
add_action( 'widgets_init', 'register_stm_recent_posts_category_widget' );