    <div class="uou-block-1c">
	   <div class="container">
      <a href="<?php echo esc_url(site_url('/')); ?>" class="logo"> <img src="<?php echo esc_url(medicaldirectory_IMAGE); ?>logo.png" alt="<?php esc_html_e( 'image', 'medical-directory' ); ?>"> </a>

      <div class="search">
        <?php get_search_form(); ?>
      </div>
      	<?php get_template_part('templates/header','socialButton'); ?>  
    </div>
  </div> <!-- end .uou-block-1a -->

