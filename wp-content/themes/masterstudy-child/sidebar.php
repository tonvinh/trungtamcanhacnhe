<div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
    <div class="sidebar-area sidebar-area-right">
        <div class="vc_row wpb_row vc_row-fluid">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="wpb_wrapper">
                    <div class="wpb_widgetised_column wpb_content_element">
                        

                        <!--Dich Vu-->
                      
                        <div class="wpb_wrapper">

                            <aside id="stm_widget_top_rated_products-3"
                                   class="widget stm_widget_top_rated_products">
                                <div class="widget_title"><h3><?php echo __('Service', 'trungtam')?></h3></div>
                                <?php
                                $args = array(
                                    'child_of' => 645
                                );
                                $data_post = get_pages($args);
                                ?>
                                <ul class="stm_product_list_widget widget_woo_stm_style_2">
                                    <?php foreach ($data_post as $item): ?>
                                        <li>
                                            <a href="<?php echo get_permalink($item->ID)?>"
                                               title="<?php echo $item->post_title; ?>">
                                                <img class="img-responsive"
                                                     src="<?php echo get_the_post_thumbnail_url($item->ID, 'sidebar_img')?>">
                                                <div class="meta">
                                                    <div class="title h5"><?php echo $item->post_title; ?></div>
                                                </div>
                                            </a>
                                        </li>
                                    <?php endforeach;?>
                                </ul>
                            </aside>
                        </div>
						<br>
						<div class="wpb_wrapper">

                            <aside id="stm_widget_top_rated_products-3"
                                   class="widget stm_widget_top_rated_products">
                                <div class="widget_title"><h3><?php echo __('Activity', 'trungtam')?></h3></div>
                                <?php
                                $args = array(
                                    'post_type' => 'post',
                                    'posts_per_page' => 5,

                                );
                                $data_post = get_posts($args);
                                ?>
                                <ul class="stm_product_list_widget widget_woo_stm_style_2">
                                    <?php foreach ($data_post as $item): ?>
                                    <li>
                                        <a href="<?php echo get_permalink($item->ID)?>"
                                           title="<?php echo $item->post_title; ?>">
                                            <img class="img-responsive"
                                                 src="<?php echo get_the_post_thumbnail_url($item->ID, 'sidebar_img')?>">
                                            <div class="meta">
                                                <div class="title h5"><?php echo $item->post_title; ?></div>
                                            </div>
                                        </a>
                                    </li>
                                    <?php endforeach;?>
                                </ul>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>