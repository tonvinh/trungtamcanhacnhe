<?php
/**
 * Template Name: Home Template
 *
 */
 get_header();?>
	<?php get_template_part('partials/title_box'); ?>
	<div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php if ( have_posts() ) :
					while ( have_posts() ) : the_post();
						the_content();
					endwhile;
				endif; ?>
            </div>
           
        </div>
    </div>  
<?php get_footer();?>