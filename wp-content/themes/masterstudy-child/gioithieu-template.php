<?php
/**
 * Template Name: Sidebar Template
 *
 */
 get_header();
?>
<?php get_template_part('partials/title_box'); ?>
<div class="container blog_main_layout_list">
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <?php if ( have_posts() ) :
                while ( have_posts() ) : the_post();
                    the_content();
                endwhile;
            endif; ?>
        </div>
        <?php get_sidebar(); ?>

    </div>
</div>
<?php get_footer(); ?>
