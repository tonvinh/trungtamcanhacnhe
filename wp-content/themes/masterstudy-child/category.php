<?php get_header();
$category = get_queried_object();

?>
    <div class="container">
        <div class="row">

             <h2 class="archive-course-title"><?php echo single_cat_title()?></h2>
                    <div class="stm_archive_product_inner_grid_content">

                            <div class="sidebar_position_none">


                                    <div class="row">
                                        <?php while (have_posts()): the_post(); ?>
                                            <div class="col-md-3 col-sm-4 col-xs-6 teacher-col event-col">
                                                <div class="event_archive_item">
                                                    <a href="<?php the_permalink(); ?>" title="View full">
                                                        <div class="event_img">
                                                            <img width="270" height="153" src="<?php echo get_the_post_thumbnail_url($post->ID,'img_post'); ?>" class="attachment-img-270-153 size-img-270-153 wp-post-image" alt="Conscious-Discipline-Summer-Institute">
                                                        </div>
                                                        <div class="h4 title"><?php echo $post->post_title; ?></div>
                                                    </a>
                                                    <div class="multiseparator"></div>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                <?php
                                    $cat_content = get_field('cat_content', $category);
                                    if(!empty($cat_content)) {
                                        echo $cat_content;
                                    }
                                ?>


                            </div>

                    </div> <!-- stm_product_inner_grid_content -->


        </div>
    </div>
<?php get_footer(); ?>



