<?php get_header();?>
	<?php get_template_part('partials/title_box'); ?>
	<div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <?php if ( have_posts() ) :

                    // check child of Dich Vu page
                    if($post->ID == 2084) {
                        $data = get_post(2084);
                            echo '<h3>'.$data->post_title.'</h3>';
                            echo do_shortcode($data->post_content);?>

                    <?php } else {
                        while ( have_posts() ) : the_post();
                            the_content();
                        endwhile;
                    }
				endif; ?>
                <?php
                    // check and show category with id = 26 load in page with id  = 1681
                if($post->ID == 1681) {
                    $cateInfo = get_category(26);
                ?>
                    <hr>
                    <h3 class="aaa"><?php echo $cateInfo->name; ?></h3> <br>
                    <div class="stm_archive_product_inner_grid_content">
                            <div class="sidebar_position_none">
                                    <div class="row">
                    <?php
                    $data_page = get_posts(array('cat' => 26, 'numberposts' => -1));
                    if(!empty($data_page)) {
                        foreach($data_page as $item) {
                    ?>
                            <div class="col-md-3 col-sm-4 col-xs-6 teacher-col event-col">
                                <div class="event_archive_item">
                                    <a href="<?php echo get_permalink($item->ID) ?>" title="View full">
                                        <div class="event_img">
                                            <img width="270" height="153" src="<?php echo get_the_post_thumbnail_url($item->ID,'img_post'); ?>" class="attachment-img-270-153 size-img-270-153 wp-post-image" alt="Conscious-Discipline-Summer-Institute">
                                        </div>
                                        <div class="h4 title"><?php echo $item->post_title; ?></div>
                                    </a>

                                </div>
                            </div>
                    <?php }
                    } ?>
                </div></div></div>
                <?php
                }

                ?>

            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>  
<?php get_footer();?>