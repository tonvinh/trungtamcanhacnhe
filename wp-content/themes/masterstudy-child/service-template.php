<?php
/**
 * Template Name: Service Template
 *
 */

?>

<?php get_header();
   the_post();
?>
<?php get_template_part('partials/title_box'); ?>

    <div class="container blog_layout_list">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <?php
                $args = array(
                    'child_of' => $post->ID,
                    'sort_order' => 'ASC',
                    'sort_column' => 'menu_order'
                );
                $data_post = get_pages($args);
                if(!empty($data_post)):
                    foreach ($data_post as $post): setup_postdata($post);
                        ?>
                        <div class="col-md-12">
                            <div class="post_list_content_unit">
                                <h2 class="post_list_item_title"><a href="<?php echo get_permalink($post->ID); ?>" title="View full"><?php echo $post->post_title; ?></a></h2>
                               <!-- <div class="post_list_meta_unit">
                                    <div class="date-d"><?php echo get_the_time('d', $post)?></div>
                                    <div class="date-m"><?php echo get_the_time('F', $post)?></div>
                                </div> -->
                                <div class="post_list_inner_content_unit post_list_inner_content_unit_left">
                                    <?php if(has_post_thumbnail($post)): ?>
                                        <a href="<?php echo get_permalink($post->ID); ?>" title="Read more">
                                            <div class="post_list_featured_image">
                                                <img width="770" height="300" src="<?php echo get_the_post_thumbnail_url($post->ID, 'service_img')?>" class="img-responsive wp-post-image" alt="<?php echo $post->post_title; ?>">
                                            </div>
                                        </a>
                                    <?php endif; ?>
                                    <div class="post_list_item_excerpt"><?php the_excerpt() ?></div>

                                    <!-- Post cats -->

                                    <!-- Post tags -->

                                    <div class="post_list_btn_more">
                                        <a href="<?php echo get_permalink($post->ID); ?>" class="btn btn-default" title="<?php echo __('Read more', 'trungtam')?>"><?php echo __('Read more', 'trungtam')?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <?php get_sidebar(); ?>

        </div>
    </div>
<?php get_footer(); ?>