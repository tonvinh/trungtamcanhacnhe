<?php get_header(); ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-xs-9">

                <br><h2 class="archive-course-title"><?php the_title()?></h2>
                <?php if(!empty($category)): ?>
                    <div class="stm_archive_product_inner_grid_content">

                        <ul class="stm-courses row list-unstyled">
                            <?php foreach($category as $item):
                                $cat_img = get_field('cat_thumbnail', $item);

                                ?>
                                <div class="stm_woo_archive_view_type_list">
                                    <li class="col-md-12 course-col-list first post-490 product type-product status-publish has-post-thumbnail product_cat-business-management product_cat-logical-thinking sale featured shipping-taxable purchasable product-type-simple product-cat-business-management product-cat-logical-thinking instock">
                                        <div class="stm_archive_product_inner_unit heading_font">
                                            <div class="stm_archive_product_inner_unit_centered clearfix">
                                                <div class="stm_featured_product_image_list">
                                                    <a href="<?php echo get_category_link($item); ?>" title="<?php echo $item->name; ?>">
                                                        <img width="300" height="225" src="<?php echo $cat_img['sizes']['img_category']; ?>" class="img-responsive wp-post-image" alt="time-management">
                                                    </a>
                                                </div>
                                                <div class="stm_products_archive_body_list">
                                                    <h2 class="title"> <?php echo $item->name; ?></h2>
                                                    <div class="stm_archive_product_exceprt normal_font">
                                                        <p><?php echo $item->description; ?></p>
                                                    </div>
                                                    <div class="see_more h5">
                                                        <a href="<?php echo get_category_link($item); ?>">Xem thêm</a>
                                                    </div>
                                                </div>
                                            </div> <!-- stm_archive_product_inner_unit_centered -->

                                        </div> <!-- stm_archive_product_inner_unit -->
                                    </li>
                                </div>
                            <?php endforeach; ?>
                        </ul>

                    </div> <!-- stm_product_inner_grid_content -->
                <?php endif; ?>

            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
<?php get_footer(); ?>